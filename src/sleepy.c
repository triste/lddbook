#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/wait.h>

static ssize_t sleepy_write(struct file *file, const char __user *buf,
		size_t count, loff_t *f_pos);
static ssize_t sleepy_read(struct file *file, char __user *buf,
		size_t count, loff_t *f_pos);

MODULE_LICENSE("GPL");

static const struct file_operations sleepy_ops = {
	.read = sleepy_read,
	.write = sleepy_write,
};

static struct cdev sleepy_cdev = {
	.owner = THIS_MODULE,
};

static DECLARE_WAIT_QUEUE_HEAD(wq);
static int flag = 0;

static ssize_t
sleepy_read(struct file *file, char __user *buf, size_t count, loff_t *f_pos)
{
	ssize_t ret = 0;
	printk(KERN_WARNING "sleepy: process %i (%s) going to sleep\n",
			current->pid, current->comm);

	/* TODO: race condition */
	wait_event_interruptible(wq, flag != 0);
	flag = 0;

	printk(KERN_WARNING "sleepy: awoken %i (%s)\n",
			current->pid, current->comm);
	return ret;
}

static ssize_t
sleepy_write(struct file *file, const char __user *buf, size_t count,
		loff_t *f_pos)
{
	printk(KERN_WARNING "sleepy: process %i (%s) awakening the readers\n",
			current->pid, current->comm);
	flag = 1;
	wake_up_interruptible(&wq);
	return count;
}

static int
sleepy_init(void)
{
	dev_t dev;
	int ret = 0;

	ret = alloc_chrdev_region(&dev, 0, 1, "sleepy");
	if (ret < 0) {
		printk(KERN_WARNING "sleepy: alloc_chrdev_region\n");
		return ret;
	}

	cdev_init(&sleepy_cdev, &sleepy_ops);

	ret = cdev_add(&sleepy_cdev, dev, 1);
	if (ret < 0) {
		printk(KERN_WARNING "sleepy: cdev_add\n");
	} else {
		printk(KERN_DEBUG "sleepy: initialized\n");
	}

	return ret;
}

static void
sleepy_exit(void)
{
	printk(KERN_DEBUG "sleepy: exit\n");
}

module_init(sleepy_init);
module_exit(sleepy_exit);
