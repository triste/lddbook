#!/bin/sh

. /test/functions.sh
load_chardev sleepy 1
# check if the reading proccess is blocked
cat /dev/sleepy &
pid=$!
sleep 1
signal=9
retval=$((128 + $signal))
kill -$signal $pid
wait $pid
if [ $? -ne $retval ]
then
	echo "$? != $retval"
	exit 1
fi
# check if the reading process wakes up after writing to the device
cat /dev/sleepy &
pid=$!
sleep 1
echo "wake up" > /dev/sleepy
wait $pid
if [ $? -ne 0 ]
then
	echo "$? != 0"
	exit 1
fi
