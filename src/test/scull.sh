#!/bin/sh

. /test/functions.sh
load_chardev scull 4

for i in $(seq 0 3)
do
	for message in "hello, scull$i" "again... hey, scull$i"
	do
		echo $message > /dev/scull$i
		output=$(cat /dev/scull$i)
		if [ ! "$output" = "$message" ]
		then
			echo "\"$output\" != \"$message\""
			exit 1
		fi
	done

done
