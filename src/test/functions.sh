load_chardev() {
	module=$1
	device=$module
	devnum=$(($2 - 1))
	shift 2
	mode="664"

	modprobe $module $* || exit 1

	rm -f /dev/${device}[0-$devnum]
	major=$(awk "\$2==\"$module\" {print \$1}" /proc/devices)

	if [ $devnum -eq 0 ]
	then
		mknod /dev/${device} c $major 0
		chmod $mode /dev/$device
	else
		for i in $(seq 0 $devnum)
		do
			mknod /dev/${device}$i c $major $i
			chmod $mode /dev/${device}$i
		done
	fi
}
