#define SCULLPIPE_BUFFERSIZE (1 << 10)

#undef PDEBUG
#ifdef SCULL_DEBUG
#ifdef __KERNEL__
#define PDEBUG(fmt, ...) printk(KERN_DEBUG "scullpipe: " fmt, ##__VA_ARGS__)
#else
#define PDEBUG(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
#endif /* __KERNEL */
#else
#define PDEBUG(fmt, ...)
#endif /* SCULL_DEBUG */
