#include "linux/semaphore.h"
#include "linux/uaccess.h"
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/slab.h>

#include <linux/poll.h>

#include "scullpipe.h"

MODULE_LICENSE("GPL");

#define MIN(a, b) (a < b ? a : b)

static unsigned int buffersize = SCULLPIPE_BUFFERSIZE;
module_param(buffersize, uint, S_IRUSR | S_IWUSR);
MODULE_PARM_DESC(buffersize, "Scullpipe buffer size");

struct scull_pipe {
	wait_queue_head_t inq, outq;
	char *buffer, *end;
	unsigned int buffersize;
	char *rp, *wp;
	int nreaders, nwriters;
	struct semaphore sem;
	struct cdev cdev;
	dev_t devnum;
};

static struct scull_pipe scullpipe_dev;

static ssize_t
scullpipe_read(struct file *file, char __user *buf, size_t count, loff_t *f_pos)
{
	struct scull_pipe *dev = file->private_data;

	if (down_interruptible(&dev->sem))
		return -ERESTARTSYS;

	while (dev->rp == dev->wp) {
		up(&dev->sem);
		if (file->f_flags & O_NONBLOCK)
			return -EAGAIN;
		PDEBUG("\"%s\" reading: going to sleep\n", current->comm);
		if (wait_event_interruptible(dev->inq, (dev->rp != dev->wp)))
			return -ERESTARTSYS;
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS;
	}
	if (dev->wp > dev->rp)
		count = MIN(count, (size_t)(dev->wp - dev->rp));
	else
		count = MIN(count, (size_t)(dev->end - dev->rp));
	if (copy_to_user(buf, dev->rp, count)) {
		up(&dev->sem);
		return -EFAULT;
	}
	dev->rp += count;
	if (dev->rp == dev->end)
		dev->rp = dev->buffer;
	up(&dev->sem);

	wake_up_interruptible(&dev->outq);
	PDEBUG("\"%s\" did read %li bytes\n", current->comm, (long)count);

	return count;
}

static int
spacefree(struct scull_pipe *dev)
{
	/* XXX: lock for rp and wp? */
	if (dev->rp == dev->wp)
		return dev->buffersize - 1;

	/*  r  w    '
	 * -------|-------|
	 *  w  r  |    '
	 */
	return ((dev->rp + dev->buffersize - dev->wp) % dev->buffersize) - 1;
}

static ssize_t
scullpipe_write(struct file *file, const char __user *buf, size_t count,
    loff_t *pos)
{
	struct scull_pipe *dev = file->private_data;

	if (down_interruptible(&dev->sem))
		return -ERESTARTSYS;

	while (spacefree(dev) == 0) {
		DEFINE_WAIT(wait);

		up(&dev->sem);
		if (file->f_flags & O_NONBLOCK)
			return -EAGAIN;
		PDEBUG("\"%s\" writing: going to sleep\n", current->comm);
		prepare_to_wait(&dev->outq, &wait, TASK_INTERRUPTIBLE);
		if (spacefree(dev) == 0)
			schedule();
		finish_wait(&dev->outq, &wait);
		if (signal_pending(current))
			return -ERESTARTSYS;
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS;
	}
	count = MIN(count, (size_t)spacefree(dev));
	if (dev->wp >= dev->rp)
		count = MIN(count, (size_t)(dev->end - dev->wp));
	else
		count = MIN(count, (size_t)(dev->rp - dev->wp - 1));
	PDEBUG("Going to accept %li bytes to %p from %p\n",
	    (long)count, dev->wp, buf);
	if (copy_from_user(dev->wp, buf, count)) {
		up(&dev->sem);
		return -EFAULT;
	}
	dev->wp += count;
	if (dev->wp == dev->end)
		dev->wp = dev->buffer;
	up(&dev->sem);

	wake_up_interruptible(&dev->inq);

	return count;
}

static int
scullpipe_open(struct inode *inode, struct file *file)
{
	struct scull_pipe *dev;

	dev = container_of(inode->i_cdev, struct scull_pipe, cdev);
	file->private_data = dev;

	return 0;
}

static const struct file_operations scullpipe_ops = {
	.open = scullpipe_open,
	.read = scullpipe_read,
	.write = scullpipe_write,
};

static int
scullpipe_init(void)
{
	dev_t dev;
	int ret;

	ret = alloc_chrdev_region(&dev, 0, 1, "scullpipe");
	if (ret < 0) {
		printk(KERN_ERR "scullpipe: can't allocate character device\n");
		return ret;
	}
	scullpipe_dev.devnum = dev;

	sema_init(&scullpipe_dev.sem, 1);
	cdev_init(&scullpipe_dev.cdev, &scullpipe_ops);
	scullpipe_dev.buffer = kmalloc(buffersize, GFP_KERNEL);
	if (scullpipe_dev.buffer == NULL) {
		ret = -ENOMEM;
		goto fail_alloc;
	}
	scullpipe_dev.wp = scullpipe_dev.rp = scullpipe_dev.buffer;
	scullpipe_dev.buffersize = buffersize;
	scullpipe_dev.devnum = dev;
	scullpipe_dev.end = scullpipe_dev.buffer + buffersize;
	init_waitqueue_head(&scullpipe_dev.inq);
	init_waitqueue_head(&scullpipe_dev.outq);

	ret = cdev_add(&scullpipe_dev.cdev, dev, 1);
	if (ret < 0) {
		printk(KERN_ERR "scullpipe: can't add chardev\n");
		goto fail_cadd;
	}

	return 0;

fail_cadd:
	kfree(scullpipe_dev.buffer);
fail_alloc:
	unregister_chrdev_region(dev, 1);

	return ret;
}

static void
scullpipe_exit(void)
{
	cdev_del(&scullpipe_dev.cdev);
	kfree(scullpipe_dev.buffer);
	unregister_chrdev_region(scullpipe_dev.devnum, 1);
}

module_init(scullpipe_init);
module_exit(scullpipe_exit);
