#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>

#include "scull.h"

MODULE_LICENSE("GPL");

struct scull_qset {
	void **data;
	struct scull_qset *next;
};

struct scull_dev {
	struct scull_qset *data;
	struct cdev cdev;
	struct semaphore sem;
	unsigned long size;
	int quantum;
	int qset;
};

static unsigned int scull_major = SCULL_MAJOR;
module_param(scull_major, uint, S_IRUSR | S_IWUSR);
MODULE_PARM_DESC(scull_major, "Major device number");

static unsigned int scull_minor = SCULL_MINOR;
module_param(scull_minor, uint, S_IRUSR | S_IWUSR);
MODULE_PARM_DESC(scull_major, "Minor device number");

static unsigned int scull_quantum = SCULL_QUANTUM;
module_param(scull_quantum, uint, S_IRUSR | S_IWUSR);
MODULE_PARM_DESC(scull_quantum, "Scull quantum size");

static unsigned int scull_qset = SCULL_QSET;
module_param(scull_qset, uint, S_IRUSR | S_IWUSR);
MODULE_PARM_DESC(scull_qset, "Scull quantum set size");

static unsigned int scull_numdevs = SCULL_NUMDEVS;
module_param(scull_numdevs, uint, S_IRUSR | S_IWUSR);
MODULE_PARM_DESC(scull_numdevs, "Scull number of devices");

static struct scull_dev *scull_devs;

static int
scull_trim(struct scull_dev *dev)
{
	struct scull_qset *dptr, *next;
	unsigned int i, qset;

	qset = dev->qset;
	for (dptr = dev->data; dptr; dptr = next) {
		if (dptr->data) {
			for (i = 0; i < qset; ++i)
				kfree(dptr->data[i]);
			kfree(dptr->data);
			dptr->data = NULL;
		}
		next = dptr->next;
		kfree(dptr);
	}
	dev->size = 0;
	dev->quantum = scull_quantum;
	dev->qset = scull_qset;
	dev->data = NULL;

	return 0;
}

static int
scull_release(struct inode *inode, struct file *file)
{
	printk(KERN_INFO "scull: programm '%s' with pid '%u' released the scull%u\n",
	    current->comm, current->pid, iminor(inode));
	return 0;
}

static int
scull_open(struct inode *inode, struct file *file)
{
	struct scull_dev *dev;

	printk(KERN_WARNING "scull: program '%s' with pid '%u' opened the scull%u\n",
	    current->comm, current->pid, iminor(inode));
	dev = container_of(inode->i_cdev, struct scull_dev, cdev);
	file->private_data = dev;
	if ((file->f_flags & O_ACCMODE) == O_WRONLY) {
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS;
		scull_trim(dev);
		up(&dev->sem);
		printk(KERN_WARNING "Device is trimmed\n");
	}
	return 0;
}

static struct scull_qset *
scull_follow(struct scull_dev *dev, int item)
{
	unsigned int i;
	struct scull_qset **dptr, **next;

	dptr = next = &dev->data;
	for (i = 0; i <= item; ++i) {
		if (*next == NULL) {
			*next = kzalloc(sizeof(struct scull_qset), GFP_KERNEL);
			if (*next == NULL)
				return NULL;
		}
		dptr = next;
		next = &(*dptr)->next;
	}

	return *dptr;
}

static ssize_t
scull_write(struct file *file, const char __user *buf, size_t count,
    loff_t *f_pos)
{
	struct scull_dev *dev = file->private_data;
	int quantum = dev->quantum;
	int qset = dev->qset;
	int itemsize = quantum * qset;
	int item, rest;
	struct scull_qset *dptr;
	int s_pos, q_pos;
	ssize_t retval = -ENOMEM;

	if (down_interruptible(&dev->sem)) {
		return -ERESTARTSYS;
	}

	item = *f_pos / itemsize;
	rest = *f_pos % itemsize;
	s_pos = rest / quantum;
	q_pos = rest % quantum;

	dptr = scull_follow(dev, item);
	if (!dptr) {
		goto out;
	}
	if (!dptr->data) {
		dptr->data = kzalloc(qset * sizeof(char *), GFP_KERNEL);
		if (!dptr->data)
			goto out;
	}
	if (!dptr->data[s_pos]) {
		dptr->data[s_pos] = kmalloc(quantum, GFP_KERNEL);
		if (!dptr->data[s_pos])
			goto out;
	}
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	if (copy_from_user(dptr->data[s_pos] + q_pos, buf, count)) {
		retval = -EFAULT;
		goto out;
	}
	*f_pos += count;
	if (dev->size < *f_pos)
		dev->size = *f_pos;

	retval = count;
out:
	up(&dev->sem);
	return retval;
}

static ssize_t
scull_read(struct file *file, char __user *buf, size_t count, loff_t *pos)
{
	struct scull_qset *dptr;
	struct scull_dev *dev = file->private_data;
	int item, rest;
	int s_pos, q_pos;
	int quantum = dev->quantum;
	int qset = dev->qset;
	int itemsize = quantum * qset;
	ssize_t retval = 0;

	if (down_interruptible(&dev->sem)) {
		return -ERESTARTSYS;
	}

	if (*pos > dev->size)
		goto out;
	if (count + *pos >= dev->size)
		count = dev->size - *pos;

	item = *pos / itemsize;
	rest = *pos % itemsize;
	s_pos = rest / quantum;
	q_pos = rest % quantum;

	dptr = scull_follow(dev, item);
	if (!dptr || !dptr->data || !dptr->data[s_pos])
		goto out;

	if (count > quantum - q_pos)
		count = quantum - q_pos;

	if (copy_to_user(buf, dptr->data[s_pos] + q_pos, count)) {
		retval = -EFAULT;
		goto out;
	}
	*pos += count;
	retval = count;

out:
	up(&dev->sem);
	return retval;
}

static const struct file_operations ops = {
	.open = scull_open,
	.release = scull_release,
	.read = scull_read,
	.write = scull_write,
};

static int
scull_init(void)
{
	unsigned int i;
	int ret;
	dev_t dev;

	printk(KERN_ALERT "Hello, world\n");
	if (scull_major) {
		dev = MKDEV(scull_major, scull_minor);
		ret = register_chrdev_region(dev, scull_numdevs, "scull");
		if (ret < 0) {
			printk(KERN_WARNING "scull: chrdev registration failed with major = %u and minor = %u",
			    scull_major, scull_minor);
			return ret;
		}
	} else {
		ret = alloc_chrdev_region(&dev, scull_minor, scull_numdevs,
		    "scull");
		if (ret < 0) {
			printk(KERN_ERR "scull: chrdev allocation failed\n");
			return ret;
		}
		scull_major = MAJOR(dev);
	}
	scull_devs = kcalloc(scull_numdevs, sizeof(struct scull_dev), GFP_KERNEL);
	if (scull_devs == NULL) {
		ret = -ENOMEM;
		goto unreg;
	}
	for (i = 0; i < scull_numdevs; ++i) {
		dev_t dev;

		sema_init(&scull_devs[i].sem, 1);
		scull_devs[i].cdev.owner = THIS_MODULE;
		cdev_init(&scull_devs[i].cdev, &ops);
		dev = MKDEV(scull_major, scull_minor + i);
		ret = cdev_add(&scull_devs[i].cdev, dev, 1);
		if (ret) {
			printk(KERN_ERR "scull: can't add %i device\n", i);
			scull_numdevs = i;
			goto delete;
		}
	}
	return 0;
delete:
	for (i = 0; i < scull_numdevs; ++i)
		cdev_del(&scull_devs[i].cdev);
	kfree(scull_devs);
unreg:
	unregister_chrdev_region(dev, scull_numdevs);
	return ret;
}

static void
scull_exit(void) {
	dev_t dev;
	unsigned int i;

	for (i = 0; i < scull_numdevs; ++i) {
		cdev_del(&scull_devs[i].cdev);
		down(&scull_devs[i].sem);
		scull_trim(&scull_devs[i]);
		up(&scull_devs[i].sem);
	}
	kfree(scull_devs);

	dev = MKDEV(scull_major, scull_minor);
	unregister_chrdev_region(dev, scull_numdevs);
	printk(KERN_ALERT "Goodbye, world\n");
}

module_init(scull_init);
module_exit(scull_exit);
