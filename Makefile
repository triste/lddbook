PWD = $(shell pwd)

DEBUG ?= y
export DEBUG

LINUZ_OBJ = vmlinuz
SYSTEMMAP_OBJ = System.map
LINUX_CONFIG = linux.config

INITRAMFS_OVERLAY_CPIO = initramfs_overlay.cpio

build: $(LINUZ_OBJ) $(INITRAMFS_OVERLAY_CPIO)


TEMPDIR = _tempdir
.PHONY: $(TEMPDIR)
# Clean if exits
$(TEMPDIR):
	[ -d $@ ] && find $@ -mindepth 1 -delete || mkdir $@


LINUX_VERSION ?= 5.12.9
LINUX_SRCDIR = linux-$(LINUX_VERSION)
LINUX_TARXZ = $(LINUX_SRCDIR).tar.xz
LINUX_URL = https://cdn.kernel.org/pub/linux/kernel/v5.x/$(LINUX_TARXZ)

$(LINUX_TARXZ):
	wget -nc $(LINUX_URL)

$(LINUX_SRCDIR): $(LINUX_TARXZ)
	tar -xJvf $<

$(LINUZ_OBJ) $(SYSTEMMAP_OBJ): $(LINUX_SRCDIR) $(LINUX_CONFIG)
	rm -f $(LINUZ_OBJ) $(SYSTEMMAP_OBJ) # stop generating *.old files
	$(MAKE) -C $< allnoconfig KCONFIG_ALLCONFIG=$(PWD)/$(LINUX_CONFIG)
	$(MAKE) -C $< -j$(shell nproc)
	$(MAKE) -C $< install INSTALL_PATH=$(PWD)


BUSYBOX_VERSION ?= 1.33.1
BUSYBOX_SRCDIR = busybox-$(BUSYBOX_VERSION)
BUSYBOX_TARBZ = $(BUSYBOX_SRCDIR).tar.bz2
BUSYBOX_URL = https://busybox.net/downloads/$(BUSYBOX_TARBZ)

$(BUSYBOX_TARBZ):
	wget -nc $(BUSYBOX_URL)

$(BUSYBOX_SRCDIR): $(BUSYBOX_TARBZ)
	tar -xjvf $<


BUSYBOX_CPIO = busybox.cpio
$(BUSYBOX_CPIO): $(BUSYBOX_SRCDIR)
	$(MAKE) -C $< defconfig
	sleep 1 && sed -i '/CONFIG_STATIC[ =]/s/.*/CONFIG_STATIC=y/' $</.config
	$(MAKE) -C $< CC=musl-gcc -j$(shell nproc) install
	cd $</_install && find . -print0 \
		| cpio --null -ov --format=newc > $(PWD)/$@


INIT_SRC = init
INITRAMFS_CPIO = initramfs.cpio
$(INITRAMFS_CPIO): $(INIT_SRC)  $(BUSYBOX_CPIO)
	cp $(BUSYBOX_CPIO) $@
	echo $(INIT_SRC) | cpio -ov --format=newc -AF $@



$(INITRAMFS_OVERLAY_CPIO): $(LINUX_SRCDIR) $(INITRAMFS_CPIO) $(TEMPDIR)
	$(MAKE) -C src KDIR=$(PWD)/$<
	$(MAKE) -C src KDIR=$(PWD)/$< modules_install \
		INSTALL_MOD_PATH=$(PWD)/$(TEMPDIR)
	$(MAKE) -C src overlay_install INSTALL_OVERLAY_PATH=$(PWD)/$(TEMPDIR)
	-cp -R testpayload/* $(TEMPDIR)
	cp $(INITRAMFS_CPIO) $@
	cd $(TEMPDIR) && find . -print0 \
		| cpio --null -ov --format=newc -AF $(PWD)/$@


#INITRAMFS_WITHMODS_TGZ = initramfs_overlay.tgz
#$(INITRAMFS_WITHMODS_TGZ): $(INITRAMFS_OVERLAY_CPIO)
#	gzip -9 -c $< > $@

cmdline = console=ttyS0
ifeq ($(DEBUG),y)
	cmdline += loglevel=8
endif

run: build
	qemu-system-x86_64 \
		-display none \
		-serial mon:stdio \
		-serial pty \
		-serial pty \
		-serial pty \
		-enable-kvm -cpu host -smp 1 -m 256M \
		-kernel $(LINUZ_OBJ) \
		-initrd $(INITRAMFS_OVERLAY_CPIO) \
		-append "$(cmdline)" \
		-s

gdb:
	gdb $(LINUX_SRCDIR)/vmlinux \
		-iex "set auto-load safe-path $(PWD)/$(LINUX_SRCDIR)" \
		-ex "target remote :1234" \
		-ex "lx-symbols"

clean:
	$(MAKE) -C src clean
	rm -f $(BUSYBOX_CPIO) $(INITRAMFS_CPIO) $(INITRAMFS_OVERLAY_CPIO)
	rm -f $(LINUZ_OBJ) $(SYSTEMMAP_OBJ)
	rm -rf $(TEMPDIR)

clean_all: clean
	rm -f $(BUSYBOX_TARBZ) $(LINUX_TARXZ)
	rm -rf $(BUSYBOX_SRCDIR) $(LINUX_SRCDIR)

.PHONY: build run clean clean_all
